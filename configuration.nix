{ config, lib, pkgs, inputs, ... }:

let
  userConfig = import "/nix/config.nix";
  mommySettings = import ./home/mommy.nix;
  bgrtModified = import ./bgrt-modified.nix;
in
{
  imports = [
    ./hardware-configuration.nix
  ];

  nix.settings.experimental-features = [ "nix-command" "flakes" ];

  boot.loader.systemd-boot.enable = true;
  boot.loader.systemd-boot.configurationLimit = 50;
  boot.loader.timeout = 0;
  boot.loader.efi.canTouchEfiVariables = true;
  boot.initrd.systemd.enable = true;
  boot.plymouth = {
    enable = true;
    # theme = "bgrt-modified";
    # themePackages = with pkgs; [
    #   (callPackage bgrtModified { })
    # ];
  };

  programs.zsh.enable = true;
  users.defaultUserShell = pkgs.zsh;
  users.users.${userConfig.username} = {
    isNormalUser = true;
    initialPassword = userConfig.userPassword;
    createHome = true;
    home = "/home/${userConfig.username}";
    extraGroups = [ "wheel" "wireshark" ];
  };

  security.polkit.enable = true;
  security.doas.enable = true;
  security.sudo.enable = false;
  security.doas.extraRules = [{
    users = [ "${userConfig.username}" "root" ];
    keepEnv = true;
    persist = true;
  }];

  fileSystems."/nix".neededForBoot = true;
  environment.persistence."/nix/system" = {
    hideMounts = true;
    directories = [
      "/etc/nixos"
      "/var/log"
      "/var/lib/bluetooth"
      "/var/lib/nixos"
      "/var/lib/systemd/coredump"
      "/etc/NetworkManager/system-connections"
    ];
    files = [
      "/etc/machine-id"
    ];
  };
  networking.hostName = userConfig.hostName;

  networking.networkmanager.enable = true;

  security.pam.services.hyprlock = { };
  security.rtkit.enable = true;
  services.pipewire = {
    enable = true;
    alsa.enable = true;
    alsa.support32Bit = true;
    pulse.enable = true;
    jack.enable = true;
  };

  services.tzupdate.enable = true;
  systemd.services.tzupdate.wantedBy = [ "multi-user.target" ];
  systemd.timers."tzupdate" = {
    wantedBy = [ "timers.target" ];
    timerConfig = {
      OnBootSec = "1h";
      OnUnitActiveSec = "1h";
      Unit = "tzupdate.service";
      Persistent = true;
    };
  };
  services.upower.enable = true;

  i18n.defaultLocale = "en_US.UTF-8";

  fonts.packages = with pkgs; [
    (nerdfonts.override { fonts = [ "Iosevka" "IosevkaTerm" "IosevkaTermSlab" ]; })
    noto-fonts
    noto-fonts-cjk
    noto-fonts-emoji
    font-awesome
    source-han-sans
    source-han-sans-japanese
    source-han-serif-japanese
  ];
  fonts.fontconfig = {
    defaultFonts = {
      serif = [ "IosevkaTermSlab Nerd Font Propo" "Noto Serif" "Source Han Serif" ];
      sansSerif = [ "Iosevka Nerd Font Propo" "Noto Sans" "Source Han Sans" ];
      monospace = [ "IosevkaTerm Nerd Font Mono" ];
    };
  };
  fonts.fontDir.enable = true;

  programs.wireshark.enable = true;
  programs.wireshark.package = pkgs.wireshark;
  environment.systemPackages = with pkgs; [
    wget
    killall
    file
    cifs-utils
    neofetch
    (mommy.override {
      inherit mommySettings;
    })
    du-dust
    libnotify
    ffmpeg
    git
    plymouth
  ];

  fileSystems."/home/${userConfig.username}/Share" = {
    device = "//donna/share";
    fsType = "cifs";
    options =
      let
        automountOpts = "x-systemd.automount,noauto,x-systemd.idle-timeout=60,x-systemd.device-timeout=5s,x-systemd.mount-timeout=5s";
      in
      [ "${automountOpts},credentials=/home/${userConfig.username}/.config/smb-secrets,uid=1000,gid=100" ];
  };

  programs.hyprland.enable = true;
  programs.mtr.enable = true;

  services.greetd = {
    enable = true;
    settings = {
      default_session = {
        command = "${pkgs.hyprland}/bin/Hyprland > /dev/null";
        user = userConfig.username;
      };
    };
  };

  programs.fuse.userAllowOther = true;
  home-manager = {
    extraSpecialArgs = { inherit inputs; };
    users.${userConfig.username}.imports = [
      ./home.nix
      ./shared-home.nix
    ];
    users."root".imports = [
      ./root-home.nix
      ./shared-home.nix
    ];
  };

  system.stateVersion = "23.11";
}
