{ pkgs, inputs, config, ... }:

let
  userConfig = import /nix/config.nix;
in
{
  home.packages = with pkgs; [
    unzip
  ];

  programs.git = {
    enable = true;
    signing.key = userConfig.gitSigningKeygrip;
    signing.signByDefault = true;
    userEmail = userConfig.gitEmail;
    userName = userConfig.gitName;
    extraConfig = {
      init.defaultBranch = "main";
      pull.rebase = false;
    };
  };

  programs.gpg.enable = true;
  programs.gpg.homedir = "/home/${userConfig.username}/.local/share/gnupg";
  services.gpg-agent = {
    enable = true;
    enableSshSupport = true;
    defaultCacheTtl = 1800;
    pinentryFlavor = "tty";
    sshKeys = userConfig.gpgKeygripsForSsh;
  };

  programs.zsh = {
    enable = true;
    dotDir = ".config/zsh";
    enableAutosuggestions = true;
    syntaxHighlighting.enable = true;
    shellAliases = {
      update = "doas nixos-rebuild --impure switch --flake /etc/nixos#default";
      ls = "eza";
      grep = "rg";
      cat = "bat";
      nix-shell = "nix-shell --command 'zsh'";
    };
    autocd = true;
    history.size = 10000;
    history.path = "${config.home.homeDirectory}/.cache/zsh-history";
    localVariables = {
      PS1 = "%F{green}%n@%m %F{blue}%~ %#%f ";
    };
    initExtra = ''
      set -o PROMPT_SUBST
      RPS1='$(mommy -1 -s $?)'
    '';
  };

  programs.zoxide.enable = true;
  programs.bat.enable = true;
  programs.bottom.enable = true;
  programs.ripgrep.enable = true;
  programs.eza = {
    enable = true;
    git = true;
    icons = true;
  };
  programs.yt-dlp.enable = true;
}
