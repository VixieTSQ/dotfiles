{ pkgs, inputs, config, ... }:

{
  imports = [ ];

  programs.home-manager.enable = true;
  home.stateVersion = "23.11";
}
