{ config, pkgs, lib, ... }:

let
  userConfig = import /nix/config.nix;
in
{
  imports =
    let
      mapDirAttr = (x: { directory = x; mode = "0700"; user = "${userConfig.username}"; });
      mapFileAttr = (x: { file = x; parentDirectory = { mode = "700"; user = "${userConfig.username}"; }; });
      mapConf = (files: dirs: {
        environment.persistence."/nix" = {
          hideMounts = true;
          users.${userConfig.username} = {
            directories = pkgs.lib.lists.map mapDirAttr dirs;
            files = pkgs.lib.lists.map mapFileAttr files;
          };
        };
      });
    in
    [
      (
        mapConf [
          ".cache/zsh-history"
        ] [
          "Downloads"
          "Desktop"
          "Documents"
          ".mozilla/firefox"
          ".local/share/gnupg"
          ".local/share/pass"
          ".local/share/rustup"
          ".local/share/zoxide"
          ".cache/cargo"
          ".config/VSCodium/User/"
          ".ssh"
        ]
      )
    ];

  systemd.tmpfiles.rules = [
    "d! /home/${userConfig.username} 0700 ${userConfig.username} users"
    "d! /home/${userConfig.username}/.mozilla/ 0755 ${userConfig.username} users"
    "d! /home/${userConfig.username}/.config 0755 ${userConfig.username} users"
    "d! /home/${userConfig.username}/.config/dconf 0755 ${userConfig.username} users"
    "d! /home/${userConfig.username}/.local 0755 ${userConfig.username} users"
    "z! /home/${userConfig.username}/.mozilla 0755 ${userConfig.username} users"
    "Z! /home/${userConfig.username}/.config/{dconf,obs-studio,VSCodium} 0755 ${userConfig.username} users"
    "z! /home/${userConfig.username}/.config 0755 ${userConfig.username} users"
    "Z! /home/${userConfig.username}/.cache 0755 ${userConfig.username} users"
    "Z! /home/${userConfig.username}/.local/{state,share} 0755 ${userConfig.username} users"
    "z! /home/${userConfig.username}/.local/share/gnupg 0700 ${userConfig.username} users"
    "Z! /home/${userConfig.username}/.local/share/gnupg/* 0600 ${userConfig.username} users"
    "z! /home/${userConfig.username}/.local 0755 ${userConfig.username} users"
    "z! /home/${userConfig.username} 0700 ${userConfig.username} users"
    "f /home/${userConfig.username}/.local/state/nix/profiles/tmp/manifest.json 0755 ${userConfig.username} users"
    "L /home/${userConfig.username}/.local/state/nix/profiles/profile 0755 ${userConfig.username} users - /home/${userConfig.username}/.local/state/nix/profiles/tmp"
    "L /home/${userConfig.username}/.local/state/nix/profiles/home-manager 0755 ${userConfig.username} users - /home/${userConfig.username}/.local/state/nix/profiles/tmp"
    "L /home/${userConfig.username}/.nix-profile 0755 ${userConfig.username} users - /home/${userConfig.username}/.local/state/nix/profiles/profile"
    "f /home/${userConfig.username}/.nix-profile/manifest.json 0755 ${userConfig.username} users"
  ];
}
