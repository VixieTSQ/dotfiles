import { notificationPopups } from "./notifications.js";
import { indicator } from "./indicators.js";
import { bar } from "./bar.js";
import { panel } from "./panel.js";

App.config({
    style: App.configDir + "/styles.css",
    windows: [
        indicator,
        notificationPopups,
        panel,
        bar,
    ]
})

App.closeWindow("panel");
