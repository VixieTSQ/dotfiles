import { audio, backlight, getVolumeIcon, getBrightnessIcon, getSliderColorClass } from "./lib.js";

let latestIndicatorOpen = 0;
let indicatorTimeout;
let indicatorIsBrightness = false;

const openIndicator = () => {
    if (latestIndicatorOpen > 4) {
        App.openWindow("indicator");
        if (indicatorTimeout !== undefined) clearTimeout(indicatorTimeout);
        indicatorTimeout = setTimeout(() => {
            App.closeWindow("indicator");
        }, 1500);
    } else {
        latestIndicatorOpen += 1;
        if (App.windows.find((window) => window.name === "indicator") !== undefined)
            App.closeWindow("indicator");
    };
}

const indicatorIcon = Widget.Label({ vpack: "center" });
const slider = Widget.Slider({
    min: 0,
    max: 1,
    value: Utils.merge([audio.speaker.bind("volume"), audio.speaker.bind("is_muted")], () => {
        indicatorIsBrightness = false;

        if (App.windows.find((window) => window.name === "indicator"))
            slider.class_name = getSliderColorClass(audio.speaker.volume);

        indicatorIcon.label = getVolumeIcon();
        openIndicator();
        return audio.speaker.volume;
    }),
    hexpand: true,
    vpack: "center",
    onChange: ({ value }) => {
        if (indicatorIsBrightness) {
            backlight.screen_value = value;
            openIndicator();
        } else {
            audio.speaker.volume = value;
            openIndicator();
        };
    }
}).hook(backlight, (self) => {
    indicatorIsBrightness = true;
    self.class_name = getSliderColorClass(backlight.screen_value);
    self.value = backlight.screen_value;
    indicatorIcon.label = getBrightnessIcon();
    openIndicator();
})

const indicatorBox = Widget.Box({
    className: "indicator",
    homogeneous: false,
    spacing: 4,
    children: [
        indicatorIcon,
        slider
    ]
});

export const indicator = Widget.Window({
    name: 'indicator',
    anchor: ['bottom'],
    child: indicatorBox,
});