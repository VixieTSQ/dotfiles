import { panel } from "./panel.js";

export const audio = await Service.import('audio');
export const battery = await Service.import('battery')
export const hyprland = await Service.import('hyprland')
export const network = await Service.import('network')
export const systemtray = await Service.import('systemtray')

export const notifications = await Service.import('notifications')
notifications.popupTimeout = 10000;
notifications.forceTimeout = true;

export const panelOpen = Variable(false);
let panelCloseTimeout;
panelOpen.connect(undefined, ({ value }) => {
    if (value) {
        clearTimeout(panelCloseTimeout);
        App.openWindow("panel");
        panel.child.child.reveal_child = true;
    } else {
        panel.child.child.reveal_child = false;
        panelCloseTimeout = setTimeout(() => {
            App.closeWindow("panel");
        }, 300);
    }
});
globalThis.panelOpen = panelOpen;

class BrightnessService extends Service {
    static {
        Service.register(
            this,
            {
                'screen-changed': ['float'],
            },
            {
                'screen-value': ['float', 'rw'],
            },
        );
    }

    #interface = Utils.exec("sh -c 'ls -w1 /sys/class/backlight | head -1'");

    #screenValue = 0;
    #max = Number(Utils.exec('brightnessctl -e max'));

    get screen_value() {
        return this.#screenValue;
    }

    set screen_value(percent) {
        if (percent < 0)
            percent = 0;

        if (percent > 1)
            percent = 1;

        Utils.execAsync(`brightnessctl -e set ${percent * 100}% -q`);
    }

    constructor() {
        super();

        const brightness = `/sys/class/backlight/${this.#interface}/brightness`;
        Utils.monitorFile(brightness, () => this.#onChange());

        this.#onChange();
    }

    #onChange() {
        this.#screenValue = Math.pow(Number(Utils.exec('brightnessctl -e get')) / this.#max, 0.25);

        this.emit('changed');
        this.notify('screen-value');

        this.emit('screen-changed', this.#screenValue);
    }

    connect(event = 'screen-changed', callback) {
        return super.connect(event, callback);
    }
}

export const backlight = new BrightnessService;

export const getVolumeIcon = () => {
    const volume = audio.speaker.volume * 100;

    if (audio.speaker.stream?.isMuted) {
        return "󰖁"
    } else if (volume <= 35) {
        return "󰕿"
    } else if (volume <= 80) {
        return "󰖀"
    } else {
        return "󰕾"
    }
}

export const getBrightnessIcon = () => {
    const brightness = backlight.screen_value * 100;

    if (brightness <= 35) {
        return "󰃞"
    } else if (brightness <= 80) {
        return "󰃟"
    } else {
        return "󰃠"
    }
}

export const getSliderColorClass = (value) => {
    value = value * 100
    if (value <= 35) {
        return "low"
    } else if (value <= 80) {
        return "medium"
    } else {
        return "high"
    }
}

export const getBatteryIcon = () => {
    const charge = battery.percent;

    if (!battery.charging) {
        if (charge <= 5) {
            return "󰂃"
        } else if (charge <= 10) {
            return "󰁺"
        } else if (charge <= 20) {
            return "󰁻"
        } else if (charge <= 30) {
            return "󰁼"
        } else if (charge <= 40) {
            return "󰁽"
        } else if (charge <= 50) {
            return "󰁾"
        } else if (charge <= 60) {
            return "󰁿"
        } else if (charge <= 70) {
            return "󰂀"
        } else if (charge <= 80) {
            return "󰂁"
        } else if (charge <= 90) {
            return "󰂂"
        } else {
            return "󰁹"
        }
    } else {
        if (charge <= 10) {
            return "󰢜"
        } else if (charge <= 20) {
            return "󰂆"
        } else if (charge <= 30) {
            return "󰂇"
        } else if (charge <= 40) {
            return "󰂈"
        } else if (charge <= 50) {
            return "󰢝"
        } else if (charge <= 60) {
            return "󰂉"
        } else if (charge <= 70) {
            return "󰢞"
        } else if (charge <= 80) {
            return "󰂊"
        } else if (charge <= 90) {
            return "󰂋"
        } else {
            return "󰁹"
        }
    }
}

let isLowBatteryCharge = false;
battery.connect("changed", (battery) => {
    if (battery.percent <= 10 && !isLowBatteryCharge && !battery.charging) {
        isLowBatteryCharge = true;
        Utils.notify({
            summary: "Low Battery",
            body: "Charge now",
            iconName: battery.icon_name,
            urgency: "critical",
            transient: true,
            timeout: 100000
        });
    } else if (battery.percent > 15) {
        isLowBatteryCharge = false;
    }
})
