import GLib from "./types/@girs/glib-2.0/glib-2.0.js";
import { audio, battery, getBatteryIcon, getVolumeIcon, hyprland, network, systemtray } from "./lib.js";

const updateWorkspaceButtons = (self) => {
    const activeWorkspaceId = hyprland.active.workspace.id;

    self.children.forEach((button, i) => {
        if (i + 1 === activeWorkspaceId) {
            button.child.label = "\uf111"
        } else {
            const workspace = hyprland.workspaces.find((workspace) => workspace.id - 1 === i);

            if (workspace !== undefined && workspace.windows !== 0) {
                button.child.label = "󰺕"
            } else {
                button.child.label = "\uf4aa"
            };
        }
    })
}

export const bar = Widget.Window({
    name: `bar`,
    anchor: ["left", "bottom", "top"],
    exclusivity: "exclusive",
    layer: "bottom",
    child: Widget.Box({
        css: "min-width: 2rem;",
        child:
            Widget.Box({
                setup: (self) => {
                    self.hook(hyprland, (self) => {
                        const currentWorkspaceId = hyprland.active.workspace.id;
                        const currentWorkspace = hyprland.getWorkspace(currentWorkspaceId);

                        if (currentWorkspace?.windows === 1) {
                            self.visible = true;
                            self.css = "background-color: rgb(7, 54, 66);"
                        } else {
                            self.visible = !panelOpen.value;
                            self.css = "background-color: rgba(7, 54, 66, 0.75);"
                        }
                    })
                    self.hook(panelOpen, (self) => {
                        const currentWorkspaceId = hyprland.active.workspace.id;
                        const currentWorkspace = hyprland.getWorkspace(currentWorkspaceId);

                        if (currentWorkspace?.windows !== 1 || !panelOpen.value) {
                            self.visible = !panelOpen.value
                        }
                    })
                },
                className: "bar",
                child: Widget.Box({
                    vexpand: true,
                    vertical: true,
                    children: [
                        Widget.Box({
                            vertical: true,
                            spacing: 4,
                            children: Array.from({ length: 6 }).map((_, id) => Widget.Button({
                                onClicked: () => hyprland.messageAsync(`dispatch workspace ${id + 1}`),
                                child: Widget.Label("\uf4aa")
                            }))
                        }).hook(hyprland, updateWorkspaceButtons),
                        Widget.Box({
                            vexpand: true,
                            vpack: "end",
                            vertical: true,
                            spacing: 4,
                            children: [
                                Widget.Box({
                                    vertical: true,
                                    spacing: 4,
                                    /** @type {any[]} */
                                    children: []
                                }).hook(systemtray, (self, busName) => {
                                    if (busName === undefined) {
                                        return
                                    }

                                    const item = systemtray.getItem(busName);
                                    if (item === undefined) {
                                        return
                                    };

                                    self.children = [...self.children, Widget.Button({
                                        attribute: {
                                            id: item.id
                                        },
                                        child: Widget.Icon().bind('icon', item, 'icon'),
                                        tooltipMarkup: item.bind('tooltip_markup'),
                                        onPrimaryClick: (_, event) => item.activate(event),
                                        onSecondaryClick: (_, event) => item.openMenu(event),
                                    })]
                                }, "added").hook(systemtray, (self, busName) => {
                                    if (busName === undefined) {
                                        return
                                    }

                                    const item = systemtray.getItem(busName);
                                    if (item === undefined) {
                                        return
                                    };

                                    const destroyMe = self.children.find((maybeDestroy) => maybeDestroy.id === item.id);
                                    if (destroyMe !== undefined) {
                                        destroyMe.destroy()
                                    }
                                }, "removed"),
                                Widget.Label({
                                    label: Utils.merge([audio.speaker.bind("volume"), audio.speaker.bind("is_muted")], () => getVolumeIcon())
                                }),
                                Widget.Label({
                                    label: Utils.merge([battery.bind("percent"), battery.bind("charging")], () => getBatteryIcon())
                                }),
                                Widget.Overlay({
                                    child: Widget.Label({
                                        label: '󰖩'
                                    }),
                                    overlay: Widget.Spinner({
                                        vpack: "end",
                                        hpack: "end",
                                        className: "wifi-spinner"
                                    })
                                }).hook(network, (self) => {
                                    if (network.wifi.internet === 'disconnected') {
                                        // @ts-ignore
                                        self.child.label = '󰖪'
                                    } else {
                                        // @ts-ignore
                                        self.child.label = '󰖩'
                                    }

                                    if (network.wifi.internet === "connecting" || (network.wifi.internet === 'disconnected' && network.wifi.enabled)) {
                                        self.overlays[0].class_name = "wifi-spinner";
                                    } else {
                                        self.overlays[0].class_name = "wifi-spinner disabled"
                                    }
                                }),
                                Widget.Label({
                                    label: GLib.DateTime.new_now_local().format("%I\n%M"),
                                    css: "font-weight: 600;",
                                    setup: (self) => self.poll(5000, (label) => {
                                        GLib.free(label.label);

                                        // @ts-ignore
                                        label.label = GLib.DateTime.new_now_local().format("%I\n%M");
                                    }),
                                })
                            ]
                        })
                    ],
                    setup: (self) => {
                        self.hook(panelOpen, (self) => {
                            self.visible = !panelOpen.value
                        })
                    }
                }),
            })
    }),
})