import Gio from "./types/@girs/gio-2.0/gio-2.0.js"
import { notifications } from "./lib.js"
import GLib from "./types/@girs/glib-2.0/glib-2.0.js"
import Gtk from "./types/@girs/gtk-3.0/gtk-3.0.js"

/** @param {import('resource:///com/github/Aylur/ags/service/notifications.js').Notification} notification */
const notificationIcon = ({ app_entry, app_icon, image }) => {
    if (image) {
        return Widget.Box({
            className: "notification-image",
            vpack: "start",
            css: `background-image: url("${image}");`
        })
    }

    let icon = "dialog-information-symbolic"
    if (Utils.lookUpIcon(app_icon))
        icon = app_icon

    if (app_entry && Utils.lookUpIcon(app_entry))
        icon = app_entry

    return Widget.Box({
        child: Widget.Icon({
            className: "notification-image",
            vpack: "start",
            icon
        }),
    })
}

const notification = (/** @type {import("types/service/notifications").Notification} */ notification) => {
    const closeNotification = () => {
        if (notification.actions.find((action) => action.id === "default") !== undefined) {
            notification.invoke("default");
        }
        notification.close();
    }

    const bodyText = notification.body.replace(" \n", " ").replace("\n", " ");
    const bodyPreview = Widget.Revealer({
        transition: 'slide_down',
        transitionDuration: 250,
        revealChild: true,
        child: Widget.Label({
            className: "notification-body",
            hpack: "start",
            use_markup: true,
            truncate: "end",
            lines: 2,
            maxWidthChars: 40,
            wrap: true,
            justification: "left",
            label: bodyText
        })
    });
    const bodyExpanded = Widget.Revealer({
        transition: 'slide_up',
        transitionDuration: 250,
        revealChild: false,
        child: Widget.Label({
            className: "notification-body",
            hpack: "start",
            use_markup: true,
            maxWidthChars: 40,
            wrap: true,
            justification: "left",
            label: bodyText
        })
    });

    const dateTime = GLib.DateTime.new_from_unix_local(notification.time)
    let time = '';
    if (dateTime.get_day_of_year() == GLib.DateTime.new_now_local().get_day_of_year())
        // @ts-ignore
        time = dateTime.format("%I:%M%P");
    else if (dateTime.get_day_of_year() == GLib.DateTime.new_now_local().get_day_of_year() - 1)
        time = 'Yesterday';
    else
        // @ts-ignore
        time = dateTime.format("%d/%m");

    let widget = Widget.Revealer({
        revealChild: false,
        transition: 'slide_down',
        transition_duration: 250,
        css: "padding-bottom: 1rem;",
        attribute: {
            /** @type {any} */
            closeTimeout: undefined,
            close: () => {
                widget.reveal_child = false;
                if (widget.attribute.closeTimeout === undefined) {
                    widget.attribute.closeTimeout = setTimeout(() => {
                        widget.destroy();
                    }, 600)
                }
            }
        },
        child: Widget.EventBox(
            {
                attribute: { id: notification.id },
                onPrimaryClickRelease: closeNotification,
                hexpand: true,
                cursor: "pointer",
                className: `notification`,
                child: Widget.Box(
                    {
                        homogeneous: false,
                        children: [
                            notificationIcon(notification),
                            Widget.Box(
                                {
                                    vertical: true,
                                    className: "notification-content",
                                    vpack: "start",
                                    homogeneous: false,
                                    children: [
                                        Widget.Box({
                                            homogeneous: false,
                                            hexpand: true,
                                            children: [
                                                Widget.Label({
                                                    hpack: "start",
                                                    className: "notification-title",
                                                    use_markup: true,
                                                    truncate: "end",
                                                    maxWidthChars: 28,
                                                    justification: "left",
                                                    label: notification.summary.replace(" \n", " ").replace("\n", " ")
                                                }),
                                                bodyText.length > 39 * 2 ?
                                                    Widget.Button({
                                                        hpack: "end",
                                                        hexpand: true,
                                                        css: "padding-right: 0.5rem",
                                                        onClicked: () => {
                                                            bodyPreview.reveal_child = !bodyPreview.reveal_child;
                                                            bodyExpanded.reveal_child = !bodyExpanded.reveal_child;
                                                        },
                                                        child: Widget.Label({
                                                            justification: "right",
                                                            label: `${time} `
                                                        })
                                                    }) : Widget.Label({
                                                        hpack: "end",
                                                        hexpand: true,
                                                        css: "padding-right: 1rem",
                                                        justification: "right",
                                                        label: `${time}`
                                                    }),
                                            ]
                                        }),
                                        bodyPreview,
                                        bodyExpanded
                                    ]
                                },
                            ),

                        ]
                    },
                ),
            },
        )
    })

    if (notification.sound_file && !notification.suppress_sound) {
        const file = Gio.File.new_for_path(notification.sound_file);
        const path = file.get_path();
        if (path !== null) {
            Utils.execAsync(["mpv", "--no-video", path]).then(() => {
                GLib.free(path)
                GLib.free(file)
            }).catch((err) => printerr(err))
        } else {
            GLib.free(file)
        }
    }

    return widget;
}

const destroyNotification = (/** @type {any} */ _, /** @type {number} */ id) => {
    let notification = notificationList.children.find(n => n.child.attribute.id === id);
    if (notification !== undefined) {
        notification.attribute.close()
    }
};

const notificationList = Widget.Box({
    vertical: true,
    className: "notification-list",
    children: notifications.popups.map(notification),
}).hook(notifications, (_, id) => {
    const n = notifications.getNotification(id)
    if (n && n.popup) {
        notificationList.children = [notification(n), ...notificationList.children];
        notificationList.children[0].reveal_child = true;
    }
}, "notified").hook(notifications, destroyNotification, "dismissed")
    .hook(notifications, destroyNotification, "closed");


export const notificationPopups = Widget.Window({
    name: `notification-popups`,
    anchor: ["top", "right"],
    child: Widget.Box({
        css: "min-width: 28rem; min-height: 2px;",
        vertical: true,
        child: notificationList,
    }),
})