{ pkgs ? import <nixpkgs> { } }:

pkgs.stdenv.mkDerivation {
  name = "bgrt-modified-plymouth";
  version = "1.0";

  src = ./bgrt-modified;

  dontConfigure = true;
  dontBuild = true;

  installPhase = ''
    runHook preInstall

    mkdir -p $out/share/plymouth/themes/bgrt-modified
    cp -r $src/* $out/share/plymouth/themes/bgrt-modified/
    substituteInPlace $out/share/plymouth/themes/bgrt-modified/*.plymouth --replace '@IMAGES@' "$out/share/plymouth/themes/bgrt-modified/images"

    runHook postInstall
  '';
}
