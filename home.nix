{ pkgs, inputs, config, ... }:

let
  userConfig = import /nix/config.nix;
  mikuCursor = pkgs.runCommand "moveUp" { } ''
    mkdir -p $out/share/icons
    ln -s ${pkgs.fetchzip {
      url = "https://github.com/supermariofps/hatsune-miku-windows-linux-cursors/archive/52ab79c7cad6656a438a40932f92ce4c282daba3.zip";
      hash = "sha256-ok12Z7pbkHjW3sei77ZHFel/fUFfI3jKFHdk0vcSHAQ=";
    }}/miku-cursor-linux $out/share/icons/Miku
  '';
  selectWallpaper = pkgs.writeShellScript "select-wallpaper.sh" ''
    ls ~/.config/wallpapers/ | sort -R | tail -1 | while read FILE; do
      ln -sf ~/.config/wallpapers/$FILE ~/.config/current-wallpaper.png
    done
  '';
in
{
  imports = [
    inputs.ags.homeManagerModules.default
    inputs.hyprlock.homeManagerModules.default
    inputs.hypridle.homeManagerModules.default
  ];

  programs.home-manager.enable = true;
  home.stateVersion = "23.11";

  home.packages = with pkgs; [
    playerctl
    brightnessctl
    grim
    slurp
    wl-clipboard
    wl-clip-persist
    rustup
    hyprpaper
    tofi
    libsForQt5.polkit-kde-agent
    nixpkgs-fmt
    pass
    nodejs
    mpv
  ];

  programs.firefox.enable = true;

  home.file.".config/smb-secrets".text = ''
    password=${userConfig.smbPassword}
    username=${userConfig.smbUsername}
  '';

  home.sessionVariables = {
    RUSTUP_HOME = "/home/${userConfig.username}/.local/share/rustup";
    CARGO_HOME = "/home/${userConfig.username}/.cache/cargo";
  };

  gtk = {
    enable = true;
    font.name = "sans-serif";
    theme.name = "SolArc-Dark";
    theme.package = pkgs.solarc-gtk-theme;
    iconTheme = {
      package = pkgs.vimix-icon-theme;
      name = "Vimix-Black";
    };
  };
  home.pointerCursor = {
    gtk.enable = true;
    name = "Miku";
    size = 96;
    package = mikuCursor;
  };

  programs.alacritty = {
    enable = true;
    settings = {
      env.TERM = "alacritty";
      window.padding = {
        x = 5;
        y = 5;
      };
      window.decorations = "none";
      window.opacity = 0.7;
      window.dynamic_padding = false;
      selection.save_to_clipboard = true;
      scrolling.history = 100000;
      font = {
        normal.style = "Regular";
        bold.style = "Bold";
        italic.style = "Italic";
        size = 12;
      };
      colors.bright = {
        black = "#002b36";
        blue = "#839496";
        cyan = "#93a1a1";
        green = "#586e75";
        magenta = "#6c71c4";
        red = "#cb4b16";
        white = "#fdf6e3";
        yellow = "#657b83";
      };
      colors.normal = {
        black = "#073642";
        blue = "#268bd2";
        cyan = "#2aa198";
        green = "#859900";
        magenta = "#d33682";
        red = "#dc322f";
        white = "#eee8d5";
        yellow = "#b58900";
      };
      colors.primary = {
        background = "#002b36";
        bright_foreground = "#93a1a1";
        dim_foreground = "#657b83";
        foreground = "#839496";
      };
    };
  };

  home.file.".config/wallpapers/" = {
    source = ./home/wallpapers;
    recursive = true;
  };
  home.file.".config/hypr/hyprpaper.conf".text = ''
    ipc = off
    preload = /home/${userConfig.username}/.config/current-wallpaper.png
    wallpaper = eDP-1, /home/${userConfig.username}/.config/current-wallpaper.png
    splash = true
  '';
  home.file.".config/tofi/config".source = ./home/tofi;
  wayland.windowManager.hyprland.enable = true;
  wayland.windowManager.hyprland.settings = {
    "$mod" = "SUPER";
    monitor = "eDP-1,highres,auto,1.566667";
    exec-once = [
      "systemctl start tzupdate.service"
      "${selectWallpaper} && hyprpaper & ags"
      "wl-clip-persist --clipboard both"
      "firefox"
      "${pkgs.libsForQt5.polkit-kde-agent}/libexec/polkit-kde-authentication-agent-1"
    ];
    env = [
      "XDG_SESSION_DESKTOP, Hyprland"
      "NIXOS_OZONE_WL, 1"
    ];
    input = {
      follow_mouse = true;
      touchpad = {
        natural_scroll = true;
        clickfinger_behavior = true;
        tap-to-click = false;
      };
    };
    general = {
      gaps_in = 5;
      gaps_out = 10;
      border_size = 3;
      "col.active_border" = "rgba(073642ff)";
      "col.inactive_border" = "rgba(002b36ff)";
      layout = "dwindle";
    };
    decoration = {
      rounding = 5;
      blur.size = 6;
    };
    animations = {
      bezier = [
        "myBezier, 0.83, 0, 0.17, 1"
        "bounce, 1,1.6,0.1,0.85"
        "idk, 0.05, 0.9, 0.1, 1.0"
      ];

      animation = [
        "windows, 1, 3, bounce, slide"
        "windowsOut, 1, 4, myBezier, popin 80%"
        "fade, 1, 3, myBezier"
        "workspaces, 1, 4, idk"
      ];
    };
    dwindle = {
      preserve_split = true;
      no_gaps_when_only = true;
    };
    xwayland = {
      force_zero_scaling = true;
    };
    misc = {
      force_default_wallpaper = 0;
      vfr = true;
    };
    bind =
      [
        "$mod, b, exec, firefox"
        "$mod, t, exec, alacritty"
        ''$mod, space, exec, ags -r "panelOpen.value = !panelOpen.value"''
        "$mod SHIFT, c, killactive,"
        "$mod SHIFT, q, exit,"
        "$mod, v, togglefloating,"
        "$mod, left, movefocus, l"
        "$mod, down, movefocus, d"
        "$mod, up, movefocus, u"
        "$mod, right, movefocus, r"
        "$mod, r, exec, hyprctl dispatch exec \"zsh -c \"$(tofi-drun --font /run/current-system/sw/share/X11/fonts/IosevkaTermNerdFontMono-Medium.ttf)\"\""
        '', Print, exec, grim -g "$(slurp)" -t png - | wl-copy -t image/png''
        ", XF86AudioPlay, exec, playerctl play-pause"
        ", XF86AudioNext, exec, playerctl next"
        ", XF86AudioPrev, exec, playerctl previous"
        ", XF86audiostop, exec, playerctl stop"
      ]
      ++ (
        builtins.concatLists (builtins.genList
          (
            x:
            let
              ws =
                let
                  c = (x + 1) / 6;
                in
                builtins.toString (x + 1 - (c * 6));
            in
            [
              "$mod, ${ws}, workspace, ${toString (x + 1)}"
              "$mod SHIFT, ${ws}, movetoworkspace, ${toString (x + 1)}"
            ]
          )
          6)
      );
    bindm = [
      "$mod, mouse:272, movewindow"
      "$mod, mouse:273, resizewindow"
    ];
    bindle = [
      ", XF86AudioRaiseVolume, exec, wpctl set-volume @DEFAULT_AUDIO_SINK@ 5%+"
      ", XF86AudioLowerVolume, exec, wpctl set-volume @DEFAULT_AUDIO_SINK@ 5%-"
      ", XF86MonBrightnessDown, exec, brightnessctl -e set 5%-"
      ", XF86MonBrightnessUp, exec, brightnessctl -e set 5%+"
    ];
    bindl = ", XF86AudioMute, exec, wpctl set-mute @DEFAULT_AUDIO_SINK@ toggle";
    layerrule = [
      "noanim,^(launcher)$"
      "blur,^(launcher)$"
      "blur,^(indicator)$"
      "ignorezero,^(indicator)$"
      "blur,^(notification-popups)$"
      "ignorezero,^(notification-popups)$"
    ];
    windowrule = [
      "noanim,title:^(File Upload)$"
      "float,title:^(Authentication Required.*)$"
    ];
  };

  programs.password-store = {
    enable = true;
    settings = {
      PASSWORD_STORE_DIR = "${config.xdg.dataHome}/pass";
    };
  };
  programs.zsh.initExtra = ''
    pw () {
      pass $1 || { pass find $1 ; return }
      pass $1 | grep login | cut -c 8- | wl-copy
      trash=; vared -p "Copied username" trash
      pass $1 -c
    }
  '';

  programs.ags = {
    enable = true;
    configDir = ./home/ags;
    extraPackages = with pkgs; [
      gtksourceview
      webkitgtk
      accountsservice
      upower
      networkmanager
      libdbusmenu-gtk3
    ];
  };

  programs.hyprlock = {
    enable = true;
    general.grace = 2;
    backgrounds = [{
      path = "/home/${userConfig.username}/.config/current-wallpaper.png";
      color = "rgb(eee8d5)";

      blur_size = 4;
      blur_passes = 1;
      noise = 0.0117;
      contrast = 1.3000;
      brightness = 0.8000;
      vibrancy = 0.3100;
      vibrancy_darkness = 0.0000;
    }];
    labels = [
      {
        text = "cmd[update:5000] echo \"<b><big> $(${pkgs.coreutils}/bin/date +\"%I:%M%p\") </big></b>\"";
        color = "rgb(d7dddd)";
        font_size = 84;
        font_family = "Iosevka Nerd Font Propo";
        position = {
          x = 0;
          y = 10;
        };
        halign = "center";
        valign = "center";
      }
      {
        text = "<b>Hey <span text_transform=\"capitalize\">$USER</span></b>";
        color = "rgb(d7dddd)";
        font_size = 28;
        font_family = "Iosevka Nerd Font Propo";
        position = {
          x = 0;
          y = -10;
        };
        halign = "center";
        valign = "center";
      }
      {
        text = "<b>Type to unlock!</b>";
        color = "rgb(d7dddd)";
        font_size = 24;
        font_family = "Iosevka Nerd Font Propo";
        position = {
          x = 0;
          y = 30;
        };
        halign = "center";
        valign = "bottom";
      }
    ];
    input-fields = [{
      hide_input = true;
      placeholder_text = "";
      fail_text = "";
      check_color = "rgb(42, 161, 152)";
      inner_color = "rgba(0, 0, 0, 0)";
      outer_color = "rgba(0, 0, 0, 0)";
      fail_color = "rgb(220, 50, 47)";
      fail_transition = 0;
      fade_timeout = 350;
      position = {
        x = 0;
        y = 0;
      };
      size = {
        width = 8000;
        height = 8;
      };
      rounding = 0;
      halign = "center";
      valign = "bottom";
    }];
  };
  services.hypridle = {
    enable = true;
    lockCmd = "pidof hyprlock || ${pkgs.hyprlock}/bin/hyprlock";
    beforeSleepCmd = "loginctl lock-session";
    afterSleepCmd = "${pkgs.hyprland}/bin/hyprctl dispatch dpms on";
    listeners = [
      {
        timeout = 150;
        onTimeout = "${pkgs.brightnessctl}/bin/brightnessctl -s set 10";
        onResume = "${pkgs.brightnessctl}/bin/brightnessctl -r";
      }
      {
        timeout = 300;
        onTimeout = "loginctl lock-session";
      }
      {
        timeout = 330;
        onTimeout = "${pkgs.hyprland}/bin/hyprctl dispatch dpms off";
        onResume = "${pkgs.hyprland}/bin/hyprctl dispatch dpms on";
      }
      {
        timeout = 1800;
        onTimeout = "systemctl suspend";
      }
    ];
  };

  programs.vscode = {
    enable = true;
    package = pkgs.vscodium;
    mutableExtensionsDir = false;
    extensions = with inputs.nix-vscode-extensions.extensions.x86_64-linux.vscode-marketplace; [
      dbaeumer.vscode-eslint
      bungcip.better-toml
      bradlc.vscode-tailwindcss
      formulahendry.auto-close-tag
      mikestead.dotenv
      ms-vsliveshare.vsliveshare
      qufiwefefwoyn.inline-sql-syntax
      rust-lang.rust-analyzer
      serayuzgur.crates
      streetsidesoftware.code-spell-checker
      svelte.svelte-vscode
      unifiedjs.vscode-remark
      usernamehw.errorlens
      vscode-icons-team.vscode-icons
      jnoortheen.nix-ide
    ];

    userSettings = {
      "workbench.colorTheme" = "Solarized Dark";
      "workbench.iconTheme" = "vscode-icons";
      "editor.cursorBlinking" = "smooth";
      "editor.cursorSmoothCaretAnimation" = "on";
      "window.zoomLevel" = 1.50;
      "rust-analyzer.check.command" = "clippy";
      "workbench.tree.indent" = 12;
      "explorer.sortOrder" = "filesFirst";
      "svelte.enable-ts-plugin" = true;
      "editor.fontFamily" = "IosevkaTerm Nerd Font Propo";
      "[rust]" = {
        "editor.defaultFormatter" = "rust-lang.rust-analyzer";
        "editor.formatOnSave" = true;
      };
      "editor.formatOnSave" = true;
      "explorer.confirmDragAndDrop" = false;
      "editor.fontLigatures" = true;
      "errorLens.fontWeight" = "bold";
      "vsicons.dontShowNewVersionMessage" = true;
      "workbench.editor.labelFormat" = "short";
    };
  };
}
